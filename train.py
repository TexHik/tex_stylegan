import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ["PATH"] += os.pathsep + 'C:/Users/TexHik/AppData/Local/Programs/Python/Python36/graph/bin'
import tensorflow as tf
import cv2
import numpy as np
from matplotlib.pylab import plt

from network_v3_styled import *
from datahandling import *


BATCH_SISE = 1 #must be dividable by 4
EPOCHS = 40

sessid = np.random.randint(0,10000)
print("Sessid: ",sessid)


Dataset = Images("./dataset/hentai/")

GAN = StyleGAN(resolution=64)

#GAN.load()
print("Loaded")
#заменить instancenorm на equalized learning rate
for epoch in range(EPOCHS):
    startepochttime = time.time()
    for i in range(int(Dataset.lenldr/BATCH_SISE)-1):
        startitertime = time.time()
        dataset,latents = Dataset.getImages(GAN.resolution, GAN.resolution, i*BATCH_SISE, (i+1)*BATCH_SISE)
        with tf.device("/device:GPU:0"):
            GAN.train_step(latents,dataset)
        print('Iteration: {}/{}, Images shown: {}  Time: {}'.
              format(i,int(Dataset.lenldr/BATCH_SISE), int(GAN.imgshown),time.time()-startitertime))
        if(i%5==0):
            GAN.save()
    print('\n----Epoch: {}, Time: {}\n'.
          format(epoch,time.time()-startepochttime))
#ecci

