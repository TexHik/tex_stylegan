import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.utils import plot_model
from tensorflow.keras.regularizers import Regularizer
import numpy as np
def G_logistic(D, G, latents, noise, use_path_regularization=False, pl_mean = 0):
    fake_img,latent_style = G([noise,latents], training=True)
    loss = tf.nn.softplus(-D(fake_img, training=True))
    delta_g = 0
    if (use_path_regularization):
        w_space_2 = []
        for i in range(len(latents)):
            std = 0.1 / (tf.math.reduce_std(latent_style[i], axis=0, keepdims=True) + 1e-8)
            w_space_2.append(latent_style[i] + tf.random.normal(tf.shape(latent_style[i])) / (std + 1e-8))

        # Generate from slightly adjusted W space
        pl_images,latent_style = G([noise, latents], training=True)

        # Get distance after adjustment (path length)
        delta_g = tf.reduce_mean(tf.square(pl_images - fake_img), axis=[1, 2, 3])
    if pl_mean > 0:
     loss = loss + tf.reduce_mean(tf.square(delta_g - pl_mean))

    return loss,delta_g,fake_img

def D_logistic_r1(D, G, latents, reals, noise, use_gradient_penalty=False, gamma=2.0):
    reals = tf.convert_to_tensor(reals)
    fake_images_out, _ = G([noise,latents], training=True)
    fake_scores_out = D(fake_images_out, training=True)
    real_scores_out = D(reals, training=True)
    loss = tf.nn.softplus(fake_scores_out) + tf.nn.softplus(-real_scores_out)

    if (use_gradient_penalty):
        with tf.GradientTape() as t:
            t.watch(reals)
            real_scores_out = D(reals, training=True)
            real_scored_grads = t.gradient(tf.reduce_sum(real_scores_out), [reals])[0]
            gradient_penalty = tf.reduce_sum(tf.square(real_scored_grads), axis=[1, 2, 3])
        loss = loss + gradient_penalty*gamma

    return loss,fake_scores_out,real_scores_out



