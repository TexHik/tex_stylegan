from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.utils import plot_model
from tensorflow.keras.constraints import Constraint
from tensorflow.keras.constraints import max_norm
from tensorflow.keras.initializers import Initializer
from tensorflow.keras import backend as K
from tensorflow.keras import initializers, regularizers, constraints
from tensorflow.keras.layers import Layer, InputSpec
from tensorflow.python.keras.utils import conv_utils
import tensorflow as tf
import numpy as np


class InstanceNorm(tf.keras.layers.Layer):
    def __init__(self):
        super(InstanceNorm, self).__init__()
    def build(self, input_shape):
        super(InstanceNorm, self).build(input_shape)
    def call(self, input):
            input -= tf.reduce_mean(input, axis=[1, 2], keepdims=True)
            input *= tf.math.rsqrt(tf.reduce_mean(tf.square(input), axis=[1,2], keepdims=True) + 1e-8)
            return input
class AddNoise(tf.keras.layers.Layer):
    def __init__(self, name=None, seed=None):
        self.seed = None
        super(AddNoise, self).__init__(name=name)
    def build(self, input_shape):
        self.noiseweight = self.add_weight(shape=[input_shape[3]], initializer='zeros', name='weight', trainable=True, dtype=tf.float32)
        super(AddNoise, self).build(input_shape)

    def call(self, x):
        return x+tf.random.normal([tf.shape(x)[0], x.shape[1], x.shape[2],1], dtype=x.dtype, seed=self.seed) * self.noiseweight
class AddBias(tf.keras.layers.Layer):
    def __init__(self, reg=None,name=None):
        self.reg = reg
        super(AddBias, self).__init__(name=name)
    def build(self, input_shape):
        self.bias = self.add_weight(shape=[input_shape[-1]], initializer='zeros', constraint=self.reg,
                                      name='beta')
        super(AddBias, self).build(input_shape)
    def call(self, x):
        return x + self.bias
class CoreLayer(tf.keras.layers.Layer):
    def __init__(self, size, chanells, constraint=None):
        self.size = size
        self.chanells = chanells
        self.constraint = constraint
        super(CoreLayer, self).__init__()
    def build(self,input_shape):
        self.core = self.add_weight(shape=[1, self.size,self.size,self.chanells], initializer='ones', constraint=self.constraint,
                                         name='core', trainable=True,dtype=tf.float32)


        super(CoreLayer, self).build(input_shape)
    def call(self, input):
        batch_size = tf.shape(input)[0]
        self.data = tf.tile(self.core,multiples=[batch_size, 1, 1, 1])
        return self.data
class ApplyStyle(tf.keras.layers.Layer):
    def __init__(self):
        super(ApplyStyle, self).__init__()
    def build(self,input_shape):
        super(ApplyStyle, self).build(input_shape)
    def call(self, x):
        style = tf.reshape(x[1],[-1,2,int(x[1].shape[1]/2)])
        return x[0]*style[:,0]+style[:,1]
class Blur2x2(tf.keras.layers.Layer):
    def __init__(self, filter=[1,3,3,1], stride=1, name=None):
        self.f = np.asarray(filter)
        # matrix squareing A = A*(A^T)
        self.f = self.f[:, np.newaxis] * self.f[np.newaxis, :]
        self.f = self.f/np.sum(self.f)
        # reshape (3x3 -> 3x3x1x1)
        self.f = self.f[:, :, np.newaxis, np.newaxis]
        self.f = tf.cast(self.f, tf.float32)

        self.stride = stride
        super(Blur2x2, self).__init__(name=name)
    def build(self,input_shape):
        self.f = np.tile(self.f, [1, 1, int(input_shape[3]), 1])
        super(Blur2x2, self).build(input_shape)
    def call(self, x):
        orig = x.dtype
        x = tf.cast(x, tf.float32)
        return tf.cast(tf.nn.depthwise_conv2d(x, self.f, strides=[1,self.stride,self.stride,1], padding='SAME'),orig)

class Conv2DMod(tf.keras.layers.Layer):
    def __init__(self,
                 channels,
                 kernel_size,
                 strides=1,
                 padding='valid',
                 kernel_initializer='glorot_uniform',
                 kernel_regularizer=None,
                 activity_regularizer=None,
                 kernel_constraint=None,
                 demod=True,
                 **kwargs):
        super(Conv2DMod, self).__init__(**kwargs)
        self.channels = channels
        self.rank = 2
        self.kernel_size = kernel_size
        self.strides = strides
        self.padding = padding
        self.kernel_initializer = kernel_initializer
        self.kernel_regularizer = kernel_regularizer
        self.activity_regularizer = activity_regularizer
        self.kernel_constraint = kernel_constraint
        self.demod = demod
    def build(self, input_shape):
        #[input,style]
        self.kernel = self.add_weight(shape=[self.kernel_size,self.kernel_size,input_shape[0][-1],self.channels],
                                      initializer=self.kernel_initializer,
                                      name='kernel',
                                      regularizer=self.kernel_regularizer,
                                      constraint=self.kernel_constraint)
    def call(self, inputs):
        # To channels first
        x = tf.transpose(inputs[0], [0, 3, 1, 2])

        # Get weight and bias modulations
        # Make sure w's shape is compatible with self.kernel
        w = K.expand_dims(K.expand_dims(K.expand_dims(inputs[1], axis=1), axis=1), axis=-1)
        # Add minibatch layer to weights
        wo = K.expand_dims(self.kernel, axis=0)
        # Modulate
        #print(inputs[0].shape,inputs[1].shape,wo.shape,w.shape)
        weights = wo * (w + 1)
        # Demodulate
        if self.demod:
            d = K.sqrt(K.sum(K.square(weights), axis=[1, 2, 3], keepdims=True) + 1e-8)
            weights = weights / d
        # Reshape/scale input
        x = tf.reshape(x, [1, -1, x.shape[2], x.shape[3]])  # Fused => reshape minibatch to convolution groups.
        w = tf.reshape(tf.transpose(weights, [1, 2, 3, 0, 4]),
                       [weights.shape[1], weights.shape[2], weights.shape[3], -1])
        x = tf.nn.conv2d(x, w,
                         strides=self.strides,
                         padding="SAME",
                         data_format="NCHW")
        # Reshape/scale output.
        x = tf.reshape(x, [-1, self.channels, x.shape[2],
                           x.shape[3]])  # Fused => reshape convolution groups back to minibatch.
        #To channels last
        x = tf.transpose(x, [0, 2, 3, 1])

        return x

def minibatch_stddev_layer(x, group_size=4):
    group_size = tf.minimum(group_size, tf.shape(x)[0])     # Minibatch must be divisible by (or smaller than) group_size.
    s = x.shape                                             # [NHWC]  Input shape.
    y = tf.reshape(x, [group_size, -1, s[1], s[2], s[3]])   # [GMHWC] Split minibatch into M groups of size G.
    y = tf.cast(y, tf.float32)                              # [GMHWC] Cast to FP32.
    y -= tf.reduce_mean(y, axis=0, keepdims=True)           # [GMHWC] Subtract mean over group.
    y = tf.reduce_mean(tf.square(y), axis=0)                # [MHWC]  Calc variance over group.
    y = tf.sqrt(y + 1e-8)                                   # [MHWC]  Calc stddev over group.
    y = tf.reduce_mean(y, axis=[1,2,3], keepdims=True)      # [M111]  Take average over fmaps and pixels.
    y = tf.cast(y, x.dtype)                                 # [M111]  Cast back to original data type.
    y = tf.tile(y, [group_size, s[1], s[2], 1])             # [NHW1]  Replicate over group and pixels.
    return tf.concat([x, y], axis=3)                        # [NHWC]  Append as new fmap.
def noise_bias_act(x, name=None, seed = None):
    x = AddNoise(name=name+"_noice", seed=None)(x)
    x = AddBias(reg=None,name=name+"_bias")(x)
    #x = InstanceNorm()(x)
    x = layers.LeakyReLU(0.2)(x)
    return x
def bias_act(x, name=None):
    x = AddBias(reg=None,name=name+"_bias")(x)
    x = layers.LeakyReLU(0.2)(x)
    return x


def cropnoise(x):
    height = x[1].shape[1]
    width = x[1].shape[2]
    channels = x[1].shape[3]
    return x[0][:, :height, :width, :channels]

def to_rgb(input, style, targetsize):
    x = Conv2DMod(3, 1, kernel_initializer="he_uniform", demod=False)([input, style])
    return upscale_to_size(x,targetsize)

def upscale_to_size(x,imgsize):
    up_coef = imgsize / x.shape[2]
    x = layers.UpSampling2D((up_coef, up_coef),interpolation='bilinear')(x)
    return x

def gen_block(input,istyle,inoise, channels, targetsize):
    out = layers.UpSampling2D((2, 2),interpolation='bilinear')(input)
    inoise = cropnoise([inoise,out])
    out = Conv2DMod(channels=channels,
                    kernel_size=3,
                    padding='same',
                    kernel_initializer='he_uniform')\
        ([out,layers.Dense(input.shape[-1], kernel_initializer='he_uniform')(istyle)])

    out = layers.LeakyReLU(0.2)\
        (out + layers.Dense(channels, kernel_initializer = 'zeros')(inoise))


    out = Conv2DMod(channels=channels,
                    kernel_size=3,
                    padding='same',
                    kernel_initializer='he_uniform')\
        ([out,layers.Dense(channels, kernel_initializer='he_uniform')(istyle)])
    out = layers.LeakyReLU(0.2)(layers.Add()([out, layers.Dense(channels, kernel_initializer = 'zeros')(inoise)]))
    return out, to_rgb(out,layers.Dense(channels,kernel_initializer='he_uniform')(istyle), targetsize)

def disc_block(input, channels):
    res = layers.Conv2D(channels, 1, kernel_initializer='he_uniform')(input)

    out = layers.Conv2D(channels, kernel_size = 3, padding = 'same', kernel_initializer = 'he_uniform')(input)
    out = layers.LeakyReLU(0.2)(out)

    out = layers.Conv2D(channels, kernel_size = 3, padding = 'same', kernel_initializer = 'he_uniform')(out)
    out = layers.LeakyReLU(0.2)(out)

    out = layers.AveragePooling2D()(res + out)
    return out