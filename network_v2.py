from tensorflow.keras import layers
from tensorflow.keras.constraints import max_norm
from tensorflow.keras.utils import plot_model
import cv2
import numpy as np
import os
from ops import *
from loss import *
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ["PATH"] += os.pathsep + 'C:/Users/TexHik/AppData/Local/Programs/Python/Python36/graph/bin'

class StyleGAN():
    def __init__(self,
                 base_channels=512,
                 fmap_max=256,
                 fmap_min=64,
                 resolution=512,
                 D_Lr=0.0001,
                 G_Lr=0.0001):

        self.trainstep = 0
        self.imgshown = 0
        self.writer = tf.summary.create_file_writer("./tensorboard/")

        self.seed = 123

        self.base_channels = base_channels
        self.fmap_max = fmap_max
        self.fmap_min = fmap_min
        self.resolution = resolution
        self.resolution_log2 = int(np.log2(self.resolution))
        self.num_layers = self.resolution_log2 * 2 - 2


        self.genoptimizer = tf.keras.optimizers.Adam(G_Lr, beta_1=0, beta_2=0.99)
        self.discoptimizer = tf.keras.optimizers.Adam(D_Lr, beta_1=0, beta_2=0.99)

        self.generator = self._G()
        self.discriminator = self._D()
        #Model plotting
        plot_model(self.generator, to_file='gen.png', show_shapes=True, show_layer_names=True)
        plot_model(self.discriminator, to_file='disc.png', show_shapes=True, show_layer_names=True)
    def nf(self, stage):
        return max(min(int(self.base_channels / (2.0 ** (stage))), self.fmap_max),self.fmap_min)

    def GenBlock(self, x, res, name):
        x = layers.Conv2DTranspose(self.nf(res),
                                   kernel_size=(3, 3),
                                   strides=(2, 2),
                                   padding='same',
                                   use_bias=False,
                                   kernel_initializer='he_uniform',
                                   kernel_constraint=None,
                                   name=name + '_conv0_up')(x)
        x = Blur2x2(name=name + "_UpSampleBlur")(x)
        x = noise_bias_act(x,
                           name=name + "_nba0")
        x = layers.Conv2DTranspose(self.nf(res),
                          kernel_size=(3, 3),
                          padding='same',
                          use_bias=False,
                          kernel_initializer='he_uniform',
                          kernel_constraint=None,
                          name=name + '_conv1')(x)
        x = noise_bias_act(x,
                           name=name + "_nba1")
        return x

    def DiscBlock(self,x, res, name):
        t=x
        x = layers.Conv2D(self.nf(res - 1),
                          kernel_size=(3, 3),
                          padding='same',
                          use_bias=False,
                          kernel_initializer='he_uniform',
                          kernel_constraint=None,
                          name=name+ '_conv0')(x)
        x = bias_act(x,name=name+"_ba0")
        x = Blur2x2(name='%dx%d' % (2 ** res, 2 ** res) + 'blur0')(x)
        x = layers.Conv2D(self.nf(res - 2),
                          kernel_size=(3, 3),
                          strides=(2, 2),
                          padding='same',
                          use_bias=False,
                          kernel_initializer='he_uniform',
                          kernel_constraint=None,
                          name=name+ 'conv1_down')(x)
        x = bias_act(x, name=name + "_ba1")

        t = Blur2x2(name='%dx%d' % (2 ** res, 2 ** res) + 'blur1')(t)
        t = layers.Conv2D(self.nf(res - 2),
                          kernel_size=(3, 3),
                          strides=(2, 2),
                          padding='same',
                          use_bias=False,
                          kernel_initializer='he_uniform',
                          kernel_constraint=None,
                            name = '%dx%d' % (2 ** res, 2 ** res) + 'conv1_down')(t)
        x = (x+t)*(1/np.sqrt(2))
        return x

    def torgb(self,x, y=None, name=""):
        t = layers.Conv2DTranspose(3, kernel_size=(1, 1),
                                   padding='same',
                                   use_bias=False,
                                   name=name + '_conv_torgb',
                                   kernel_initializer='he_uniform',
                                   kernel_constraint=None
								   )(x)
        t = bias_act(t,name=name+"_ba")
        return t if y is None else y+t

    def fromRGB(self,x, y, res, name=""):
        t = layers.Conv2D(self.nf(res-1), kernel_size=(1, 1),
                          padding='same',
                          use_bias=False,
                          kernel_initializer='he_uniform',
                          kernel_constraint=None,
                          name=name + 'fromrgb_%dx%d' % (2 ** res, 2 ** res))(y)
        t = bias_act(t,
                     name=name+'fromrgb_ba%dx%d' % (2 ** res, 2 ** res))
        return t if x is None else x+t


    def _G(self):
        latents = layers.Input(shape=(1,))
        core = CoreLayer(4, self.nf(0), constraint=None)(latents) #max_norm(np.sqrt(2), axis=[0,1,2])
        x = noise_bias_act(core,name='core_nba0')

        y = self.torgb(x,name='core')
        for res in range(3, self.resolution_log2 + 1):
            x = self.GenBlock(x,res,name=str(res))
            y = layers.UpSampling2D(size=(2,2))(y)
            y = self.torgb(x,y,str(res))
        output = y
        return tf.keras.Model(inputs=latents, outputs=y)
    def _D(self):
        inputs = layers.Input(shape=(self.resolution, self.resolution, 3,))

        x = None
        y = inputs
        for res in range(self.resolution_log2, 2, -1):
            x = self.fromRGB(x,y,res,str(res))
            x = self.DiscBlock(x,res,str(res))
            y = layers.AveragePooling2D(pool_size=(2,2))(y)

        x = self.fromRGB(x, y, 2, "out_")
        x = minibatch_stddev_layer(x)

        x = layers.Conv2D(self.nf(1),
                          kernel_size=(3, 3),
                          padding='same',
                          use_bias=False,
                          kernel_initializer='he_uniform',
                          kernel_constraint=None,
                          name='conv_out')(x)
        x = bias_act(x,name="out_conv0")

        x = layers.Dense(self.nf(0),
                         kernel_initializer='he_uniform',
                          kernel_constraint=None,
                         use_bias=False, name="dense_0")(x)
        x = bias_act(x, name="out_conv1")

        x = layers.Dense(1,
                         kernel_initializer='he_uniform',
                         kernel_constraint=None,
                         use_bias=False,
                         name="dense_out")(x)
        output = x
        return tf.keras.Model(inputs=inputs, outputs=output)

    def train_step(self,latents,targets):
        with tf.device("/gpu:0"):
            with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape, tf.GradientTape() as gen_tape_reg, tf.GradientTape() as disc_tape_reg:
                disc_loss, disc_reg, fake_scores_out, real_scores_out = D_logistic_r1(self.discriminator,
                                                                                      self.generator, latents, targets)
                gen_loss, _, output_samples = G_logistic(self.discriminator, self.generator, latents)

                #gen_loss=gen_loss+gen_reg
                disc_loss=disc_loss+disc_reg
                gen_grad = gen_tape.gradient(gen_loss, self.generator.trainable_variables)
                disc_grad = disc_tape.gradient(disc_loss, self.discriminator.trainable_variables)

                with self.writer.as_default():
                    tf.summary.scalar("%dx%d/Loss/gen" % (self.resolution,self.resolution), tf.reduce_mean(gen_loss),
                                      step=self.trainstep)
                    tf.summary.scalar("%dx%d/Loss/disc" % (self.resolution,self.resolution), tf.reduce_mean(disc_loss),
                                      step=self.trainstep)
                    tf.summary.scalar("%dx%d/Scores/fake" % (self.resolution,self.resolution), tf.reduce_mean(fake_scores_out),
                                      step=self.trainstep)
                    tf.summary.scalar("%dx%d/Scores/real" % (self.resolution,self.resolution), tf.reduce_mean(real_scores_out),
                                      step=self.trainstep)
                    tf.summary.scalar("%dx%d/Loss/disc_gradient_penalty" % (self.resolution,self.resolution), tf.reduce_mean(disc_reg),
                                      step=self.trainstep)
                    #tf.summary.scalar("%dx%d/Loss/gen_gradient_penalty" % (self.resolution,self.resolution), tf.reduce_sum(gen_reg),
                    #                 step=self.trainstep)
                    tf.summary.histogram("%dx%d/Histograms/real" % (self.resolution,self.resolution),real_scores_out, step=self.trainstep)
                    tf.summary.histogram("%dx%d/Histograms/fake" % (self.resolution, self.resolution), fake_scores_out, step=self.trainstep)
                    tf.summary.image("%dx%d:%d" % (output_samples[0].shape[1], output_samples[0].shape[1], self.trainstep),
                                     (output_samples[-3:] + tf.ones_like(output_samples[-3:])) / 2,
                                                step=self.trainstep, max_outputs=10)
                self.writer.flush()


                self.discoptimizer.apply_gradients(zip(disc_grad, self.discriminator.trainable_variables))
                self.genoptimizer.apply_gradients(zip(gen_grad, self.generator.trainable_variables))

                self.trainstep+=1
                self.imgshown+=latents.shape[0]

    def save(self):
        layersdata_gen = {}
        layersdata_disc = {}

        for layer in self.generator.layers:
            if (len(layer.get_weights())!=0):
                layersdata_gen.update({layer.name: layer.get_weights()})

        for layer in self.discriminator.layers:
            if (len(layer.get_weights())!=0):
                layersdata_disc.update({layer.name: layer.get_weights()})

        np.save("network.npy",[layersdata_gen,layersdata_disc,self.trainstep,self.imgshown])
    def load(self):
        data = np.load("network.npy", allow_pickle=True)
        layersdata_gen = data[0]
        layersdata_disc = data[1]
        self.trainstep = data[2]
        self.imgshown = data[3]
        for name,val in layersdata_gen.items():
            try:
                l = self.generator.get_layer(name=name)
            except:
                print("Gen Layer:", name, " not found")
            else:
                l.set_weights(val)
        for name,val in layersdata_disc.items():
            try:
                l = self.discriminator.get_layer(name=name)
            except:
                print("Disc Layer:", name, " not found")
            else:
                l.set_weights(val)


