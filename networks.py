import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.constraints import max_norm
from tensorflow.keras.utils import plot_model
import cv2
import numpy as np
import os
from ops import *
from loss import *


class StyleGAN():
    def __init__(self,
                 use_tensorboard=True,
                 base_channels=512,
                 fmap_decay=1.0,
                 fmap_max=256,
                 fmap_min=64,
                 G_reg_interval=4,
                 D_reg_interval=16,
                 resolution=512,
                 labels_count=0,
                 dlatent_size=512):
        self.use_tensorboard = use_tensorboard

        self.G_reg_interval = G_reg_interval
        self.D_reg_interval = D_reg_interval
        self.trainstep = 0
        if(self.use_tensorboard):
            self.writer = tf.summary.create_file_writer("./tensorboard/")

        self.base_channels = base_channels
        self.fmap_decay = fmap_decay
        self.fmap_max = fmap_max
        self.fmap_min = fmap_min
        self.resolution = resolution
        self.labels_count = labels_count
        self.dlatent_size = dlatent_size

        self.resolution_log2 = int(np.log2(self.resolution))
        self.num_layers = self.resolution_log2 * 2 - 2

        map_trainables = 0
        #Learning rate for different res.

        #Minibatch size for different res.
        self.genoptimizer = tf.keras.optimizers.Adam(0.00001, beta_1=0, beta_2=0.99)
        self.discoptimizer = tf.keras.optimizers.Adam(0.00001, beta_1=0, beta_2=0.99)

        self.kernel_initializer = tf.random_normal_initializer(stddev=0.02, mean=0.0)

        self.generator = self._G()
        self.discriminator = self._D()

        print("Image size: ", self.resolution)
        print(
            "Total count of generator trainable parameters: {}\n"
            "Total count of discriminator trainable parameters: {}\n"
            "Total count of mapper trainable parameters: {}\n"
                # "Total count of mapper trainable parameters: {}"
                .format(
                np.sum([np.prod(v.get_shape().as_list()) for v in self.generator.trainable_variables]),
                np.sum([np.prod(v.get_shape().as_list()) for v in self.discriminator.trainable_variables]),
                map_trainables
            )
        )
        plot_model(self.generator, to_file='gen.png', show_shapes=True, show_layer_names=True)
        plot_model(self.discriminator, to_file='disc.png', show_shapes=True, show_layer_names=True)
    def nf(self, stage):
        return max(min(int(self.base_channels / (2.0 ** (stage * self.fmap_decay))), self.fmap_max),self.fmap_min)

    def _G(self):
        latents = layers.Input(shape=(1,))
        def epiloge(x,name=None):
            x = AddNoise(name=name)(x)
            x = AddBias(reg=max_norm(np.sqrt(2)))(x)
            x = layers.LeakyReLU(0.2)(x)
            x = InstanceNorm()(x)
            return x
        def torgb(x, name):
            x = layers.Conv2DTranspose(3, kernel_size=(1, 1), padding='same', use_bias=False,
                                       name=name + 'conv_torgb', kernel_initializer=self.kernel_initializer,
                                       kernel_regularizer=max_norm(1.0, axis=[0,1,2]))(x)
            x = AddBias(reg=max_norm(np.sqrt(2)),name=name+'conv_torgb_bias')(x)
            return x
        def UpSampleBlock(x, res, name):
            x = layers.Conv2DTranspose(self.nf(res), kernel_size=(3, 3),strides=(2,2), padding='same', use_bias=False,
                                       kernel_initializer=self.kernel_initializer, kernel_constraint=max_norm(np.sqrt(2), axis=[0,1,2]),
                                       name='%s' % name + '_conv0_up')(x)
            x = Blur2x2(name=name+"_UpSampleBlur")(x)
            x = epiloge(x,name=name+"_noise0")
            x = layers.Conv2D(self.nf(res), kernel_size=(3, 3), padding='same', use_bias=False, bias_initializer='zeros',
                                       kernel_initializer=self.kernel_initializer, kernel_constraint=max_norm(np.sqrt(2), axis=[0,1,2]),
                                       name='%s' % name + '_conv1')(x)
            x = epiloge(x,name=name+"_noise1")
            return x

        core = CoreLayer(4,self.nf(0),constraint=max_norm(np.sqrt(2), axis=[0,1,2]))(latents)
        x = epiloge(core,name="corenoise")
        x = torgb(x, 'init_')

        for i in range(3, self.resolution_log2 + 1):
            img = UpSampleBlock(x, i, name='UpSampleBlock_%d' % i)
            x = torgb(img, 'ToRGB%d' % i)

        outputs = x
        return tf.keras.Model(inputs=latents, outputs=outputs)

    def _D(self):
        inputs = layers.Input(shape=(self.resolution, self.resolution, 3,))
        def DownSampleBlock(x, res):
            x = layers.Conv2D(self.nf(res - 1), kernel_size=(3, 3), padding='same', use_bias=False,
                              bias_initializer='zeros',
                              kernel_initializer=self.kernel_initializer, kernel_constraint=max_norm(np.sqrt(2), axis=[0,1,2]),
                              name='%dx%d' % (2 ** res, 2 ** res) + 'conv0')(x)
            x = AddBias(reg=max_norm(np.sqrt(2)),name='addbias0%dx%d' % (2 ** res, 2 ** res))(x)
            x = layers.LeakyReLU(0.2)(x)
            x = Blur2x2(name='%dx%d' % (2 ** res, 2 ** res) + 'blur0')(x)

            x = layers.Conv2D(self.nf(res - 2), kernel_size=(3, 3),strides=(2,2), padding='same', use_bias=False,
                              bias_initializer='zeros',
                              kernel_initializer=self.kernel_initializer, kernel_constraint=max_norm(np.sqrt(2), axis=[0,1,2]),
                              name='%dx%d' % (2 ** res, 2 ** res) + 'conv1')(x)
            x = AddBias(reg=max_norm(np.sqrt(2)),name='addbias1%dx%d' % (2 ** res, 2 ** res))(x)
            x = layers.LeakyReLU(0.2)(x)
            return x
        def fromRGB(x, res, name=""):
            x = layers.Conv2D(self.nf(res), kernel_size=(1, 1), padding='same', use_bias=False,
                              bias_initializer='zeros',
                              kernel_initializer=self.kernel_initializer, kernel_constraint=max_norm(np.sqrt(2), axis=[0,1,2]),
                              name=name + 'FromRGB_lod%dx%d' % (2 ** res, 2 ** res))(x)
            x = AddBias(reg=max_norm(np.sqrt(2)),name=name+'FromRGB%dx%d' % (2 ** res, 2 ** res))(x)
            x = layers.LeakyReLU(0.2)(x)
            return x

        x = fromRGB(inputs, self.resolution_log2, str(self.resolution_log2))
        for i in range(self.resolution_log2, 2, -1):
            img = DownSampleBlock(x, i)
            img = fromRGB(img, i - 1, str(i - 1))
            x = Blur2x2(name='%dx%d' % (2 ** i, 2 ** i) + 'lerp_blur1', filter=[0.5, 0.5], stride=2)(x)
            x = fromRGB(x, i - 1, str(i - 1) + "lerp")
            x = lerp_clip(img, x, 0.5 - (self.resolution_log2 - i))

        x = minibatch_stddev_layer(x)

        x = layers.Conv2D(self.nf(1), kernel_size=(3, 3), padding='same', use_bias=False,
                          bias_initializer='zeros',
                          kernel_initializer=self.kernel_initializer,
                          kernel_constraint=max_norm(np.sqrt(2), axis=[0,1,2]),
                          name='conv_out')(x)
        x = AddBias(reg=max_norm(np.sqrt(2)))(x)
        x = layers.LeakyReLU(0.2)(x)

        x = layers.Dense(self.nf(0), kernel_initializer=self.kernel_initializer, kernel_constraint=max_norm(np.sqrt(2)), use_bias=False, name="dense_0")(x)
        x = AddBias(name="addbias_out0",reg=max_norm(np.sqrt(2)))(x)
        x = layers.LeakyReLU(0.2)(x)

        x = layers.Dense(1, kernel_initializer=self.kernel_initializer,kernel_constraint=max_norm(1.0), use_bias=False, name="dense_out")(x)

        outputs = x
        return tf.keras.Model(inputs=inputs, outputs=outputs)

    def train_step(self, latents, targets):
            with tf.device("/gpu:0"):
                with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape,tf.GradientTape() as gen_tape_reg, tf.GradientTape() as disc_tape_reg:
                    disc_loss, disc_reg, fake_scores_out, real_scores_out = D_logistic_r1(self.discriminator,self.generator, latents, targets)
                    gen_loss, gen_reg, output_samples = G_logistic(self.discriminator, self.generator, latents)

                    gen_grad = gen_tape.gradient(gen_loss, self.generator.trainable_variables)
                    disc_grad = disc_tape.gradient(disc_loss, self.discriminator.trainable_variables)

                    if ((gen_reg is not None) and self.trainstep%self.G_reg_interval==0):
                        gen_grad_reg = gen_tape_reg.gradient(tf.reduce_mean(gen_reg * self.G_reg_interval),self.generator.trainable_variables)
                    if ((disc_reg is not None) and self.trainstep%self.D_reg_interval==0):
                        disc_grad_reg = disc_tape_reg.gradient(tf.reduce_mean(disc_reg * self.D_reg_interval),self.discriminator.trainable_variables)


                if (self.use_tensorboard):
                    with self.writer.as_default():
                        tf.summary.scalar("%dx%d/Loss/gen" % (self.resolution,self.resolution), tf.reduce_mean(gen_loss),
                                          step=self.trainstep)
                        tf.summary.scalar("%dx%d/Loss/disc" % (self.resolution,self.resolution), tf.reduce_mean(disc_loss),
                                          step=self.trainstep)
                        tf.summary.scalar("%dx%d/Scores/fake" % (self.resolution,self.resolution), tf.reduce_mean(fake_scores_out),
                                          step=self.trainstep)
                        tf.summary.scalar("%dx%d/Scores/real" % (self.resolution,self.resolution), tf.reduce_mean(real_scores_out),
                                          step=self.trainstep)

                        tf.summary.scalar("%dx%d/Loss/gradient_penalty" % (self.resolution,self.resolution), tf.reduce_mean(real_scores_out),
                                          step=self.trainstep)
                    self.writer.flush()

                self.discoptimizer.apply_gradients(zip(disc_grad, self.discriminator.trainable_variables))
                self.genoptimizer.apply_gradients(zip(gen_grad, self.generator.trainable_variables)
                                                  )
                if ((disc_reg is not None) and self.trainstep%self.D_reg_interval==0):
                    self.discoptimizer.apply_gradients(zip(disc_grad_reg, self.discriminator.trainable_variables))
                if ((gen_reg is not None) and self.trainstep % self.G_reg_interval == 0):
                    self.genoptimizer.apply_gradients(zip(gen_grad_reg, self.generator.trainable_variables))
            self.trainstep += 1
    def saveweights(self, dir):
        self.generator.save_weights(dir + "/gen/")
        if (self.labels_count != 0):
            self.mapper.save_weights(dir + "/mapp/")
        for i in range(len(self.discriminator.layers)):
            l = self.discriminator.layers[i]
            if (l.name[:5] != 'tf_op'):
                data = l.get_weights()
                if (len(data) != 0):
                    with tf.io.gfile.GFile(dir + "/disc/" + l.name, "wb") as f:
                        np.save(f, data)
    def loadweights(self, dir):
        self.generator.load_weights(dir + "/gen/")
        if (self.labels_count != 0):
            self.mapper.load_weights(dir + "/mapp/")
        for i in os.listdir(dir + "/disc/"):
            try:
                l = self.discriminator.get_layer(name=i)
            except:
                print("Layer:", i, " not found")
            else:
                with tf.io.gfile.GFile(dir + "/disc/" + i, "rb") as f:
                    data = np.load(f, allow_pickle=True)
                try:
                    l.set_weights(data)
                except:
                    None
    def saveimage(self,real):
        img = self.generator(tf.ones([1, self.fmap_max]))
        if (self.use_tensorboard):
            with self.writer.as_default():
                tf.summary.image("%dx%d:%d" % (self.resolution,self.resolution,self.trainstep), tf.concat([(img+tf.ones_like(img))/2,(real+tf.ones_like(img))/2],axis=0), step=self.trainstep, max_outputs=2)
            self.writer.flush()

