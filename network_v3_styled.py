from tensorflow.keras import layers
from tensorflow.keras.constraints import max_norm
from tensorflow.keras.utils import plot_model
import cv2
import numpy as np
import os
from ops import *
from loss import *
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ["PATH"] += os.pathsep + 'C:/Users/TexHik/AppData/Local/Programs/Python/Python36/graph/bin'



class StyleGAN():
    def __init__(self,
                 latents_maps=1,
                 base_channels=2048,
                 channels_max=256,
                 channels_min=64,
                 resolution=512,
                 D_Lr=0.0001,
                 G_Lr=0.0001
                 ):
        self.trainstep = 0
        self.imgshown = 0
        self.writer = tf.summary.create_file_writer("./tensorboard/")

        self.latents_maps = latents_maps
        self.base_channels = base_channels
        self.channels_max = channels_max
        self.channels_min = channels_min
        self.resolution = resolution
        self.resolution_log2 = int(np.log2(self.resolution))
        self.num_layers = self.resolution_log2 * 2 - 2

        self.pl_mean = 0

        self.genoptimizer = tf.keras.optimizers.Adam(G_Lr, beta_1=0, beta_2=0.99)
        self.discoptimizer = tf.keras.optimizers.Adam(D_Lr, beta_1=0, beta_2=0.99)

        self.generator = self._G()
        self.discriminator = self._D()
        # Model plotting
        plot_model(self.generator, to_file='gen.png', show_shapes=True, show_layer_names=True)
        plot_model(self.discriminator, to_file='disc.png', show_shapes=True, show_layer_names=True)

    def nf(self, stage):
        return max(min(int(self.base_channels / (2.0 ** (stage))), self.channels_max), self.channels_min)

    def _G(self):
        noise_in = layers.Input(shape=(self.resolution,self.resolution,self.nf(0)))
        latent_in = layers.Input(shape=(self.latents_maps))

        latents = layers.Dense(512)(latent_in)
        latents = layers.LeakyReLU(0.2)(latents)
        latents = layers.Dense(512)(latents)
        latents = layers.LeakyReLU(0.2)(latents)
        latents = layers.Dense(512)(latents)
        latents = layers.LeakyReLU(0.2)(latents)
        latents = layers.Dense(512)(latents)
        latent_style = layers.LeakyReLU(0.2)(latents)

        outputs = []
        x = CoreLayer(4, self.nf(0))(latents)
        img = to_rgb(x,layers.Dense(self.nf(0),kernel_initializer='he_uniform')(latent_style), self.resolution)

        outputs.append(img)
        for res in range(3, self.resolution_log2 + 1):
            x,img = gen_block(x,latent_style, noise_in, self.nf(res), self.resolution)
            outputs.append(img)

        img_out = layers.Add()(outputs)
        img_out = img_out/2+0.5

        return tf.keras.Model(inputs=[noise_in,latent_in], outputs=[img_out,latent_style])
    def _D(self):
        input_img = layers.Input(shape=(self.resolution,self.resolution,3))
        x = input_img
        for res in range(self.resolution_log2, 2, -1):
            x = disc_block(x,self.nf(res))
        x = minibatch_stddev_layer(x)
        x = layers.Flatten()(x)
        x = layers.Dense(self.nf(0), kernel_initializer='he_uniform')(x)
        x = layers.LeakyReLU(0.2)(x)
        x = layers.Dense(1, kernel_initializer='he_uniform')(x)
        output = x
        return tf.keras.Model(inputs=input_img, outputs=output)
    def save(self):
        layersdata_gen = {}
        layersdata_disc = {}

        for layer in self.generator.layers:
            if (len(layer.get_weights())!=0):
                layersdata_gen.update({layer.name: layer.get_weights()})

        for layer in self.discriminator.layers:
            if (len(layer.get_weights())!=0):
                layersdata_disc.update({layer.name: layer.get_weights()})

        np.save("network.npy",[layersdata_gen,layersdata_disc,self.trainstep,self.imgshown])
    def load(self):
        data = np.load("network.npy", allow_pickle=True)
        layersdata_gen = data[0]
        layersdata_disc = data[1]
        self.trainstep = data[2]
        self.imgshown = data[3]
        for name,val in layersdata_gen.items():
            try:
                l = self.generator.get_layer(name=name)
            except:
                print("Gen Layer:", name, " not found")
            else:
                l.set_weights(val)
        for name,val in layersdata_disc.items():
            try:
                l = self.discriminator.get_layer(name=name)
            except:
                print("Disc Layer:", name, " not found")
            else:
                l.set_weights(val)

    def train_step(self,latents,targets):
        with tf.device("/gpu:0"):
            with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape, tf.GradientTape() as gen_tape_reg, tf.GradientTape() as disc_tape_reg:
                #CFG#
                apply_gradient_penalty = self.trainstep % 2 == 0 or self.trainstep < 10000
                apply_path_reg = self.trainstep % 16 == 0
                #CFG#

                noise = tf.random.normal(shape=[len(latents),self.resolution,self.resolution,self.nf(0)],dtype=tf.dtypes.float32)

                disc_loss, fake_scores_out, real_scores_out = D_logistic_r1(self.discriminator,
                                                                            self.generator,
                                                                            latents, targets, noise,
                                                                            apply_gradient_penalty)
                gen_loss, pl_length, output_samples = G_logistic(self.discriminator,
                                                      self.generator,
                                                      latents,
                                                      noise,
                                                      apply_path_reg,
                                                      self.pl_mean)

                if (self.pl_mean == 0):
                    self.pl_mean = np.mean(pl_length)
                self.pl_mean = 0.99 * self.pl_mean + 0.01 * np.mean(pl_length)

                gen_grad = gen_tape.gradient(gen_loss, self.generator.trainable_variables)
                disc_grad = disc_tape.gradient(disc_loss, self.discriminator.trainable_variables)

                with self.writer.as_default():
                    tf.summary.scalar("%dx%d/Loss/gen" % (self.resolution,self.resolution), tf.reduce_mean(gen_loss),
                                      step=self.trainstep)
                    tf.summary.scalar("%dx%d/Loss/disc" % (self.resolution,self.resolution), tf.reduce_mean(disc_loss),
                                      step=self.trainstep)
                    tf.summary.scalar("%dx%d/Scores/fake" % (self.resolution,self.resolution), tf.reduce_mean(fake_scores_out),
                                      step=self.trainstep)
                    tf.summary.scalar("%dx%d/Scores/real" % (self.resolution,self.resolution), tf.reduce_mean(real_scores_out),
                                      step=self.trainstep)
                    tf.summary.histogram("%dx%d/Histograms/real" % (self.resolution,self.resolution),real_scores_out, step=self.trainstep)
                    tf.summary.histogram("%dx%d/Histograms/fake" % (self.resolution, self.resolution), fake_scores_out, step=self.trainstep)
                    tf.summary.image("%dx%d:%d" % (output_samples[0].shape[1], output_samples[0].shape[1], self.trainstep),
                                     (output_samples[-3:] + tf.ones_like(output_samples[-3:])) / 2,
                                                step=self.trainstep, max_outputs=10)
                self.writer.flush()


                self.discoptimizer.apply_gradients(zip(disc_grad, self.discriminator.trainable_variables))
                self.genoptimizer.apply_gradients(zip(gen_grad, self.generator.trainable_variables))

                self.trainstep+=1
                self.imgshown+=latents.shape[0]


GAN = StyleGAN()