import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ["PATH"] += os.pathsep + 'C:/Users/TexHik/AppData/Local/Programs/Python/Python36/graph/bin'
import tensorflow as tf
import cv2
import numpy as np
from matplotlib.pylab import plt
from network_v2 import *
from datahandling import *
from tensorflow.keras import layers
from tensorflow.keras.constraints import max_norm
from tensorflow.keras.utils import plot_model
import cv2
import numpy as np
import os
from ops import *
from loss import *
import json
import tensorflow as tf


class Style_Extractor():
    def __init__(self,
                 latents_maps=2,
                 base_channels=2048,
                 resolution=512,
                 Lr=0.0001,
                 ):
        self.latents_maps = latents_maps
        self.base_channels = base_channels
        self.resolution = resolution
        self.resolution_log2 = int(np.log2(self.resolution))

        self.optimizer = tf.keras.optimizers.Adam(Lr, beta_1=0, beta_2=0.99)

        self.extractor = self._Extr()
        plot_model(self.extractor, to_file='extractor.png', show_shapes=True, show_layer_names=True)

    def loss(self,x, y):
        # (None,latents)
        return tf.reduce_sum(tf.square(x - y))
    def nf(self, stage):
        return int(self.base_channels / (2.0 ** (stage)))
    def _Extr(self):
        input_img = layers.Input(shape=(self.resolution,self.resolution,3))
        x = input_img
        for res in range(self.resolution_log2, 2, -1):
            x = disc_block(x,self.nf(res))
        x = layers.Flatten()(x)
        x = layers.Dense(self.nf(0), kernel_initializer='he_uniform')(x)
        x = layers.LeakyReLU(0.2)(x)
        x = layers.Dense(self.nf(0), kernel_initializer='he_uniform')(x)
        x = layers.LeakyReLU(0.2)(x)
        x = layers.Dense(self.nf(0), kernel_initializer='he_uniform')(x)
        x = layers.LeakyReLU(0.2)(x)
        x = layers.Dense(self.nf(0), kernel_initializer='he_uniform')(x)
        x = layers.LeakyReLU(0.2)(x)
        x = layers.Dense(self.latents_maps, kernel_initializer='he_uniform')(x)
        output = x

        return tf.keras.Model(inputs=input_img, outputs=output)

    def train_step(self,images,latents):
        with tf.device("/gpu:0"):
            with tf.GradientTape() as ext_tape:
                fake_latents = self.extractor(images)
                loss = self.loss(fake_latents,latents)
                extractor_gradient = ext_tape.gradient(loss, self.extractor.trainable_variables)
                self.optimizer.apply_gradients(zip(extractor_gradient, self.extractor.trainable_variables))
        return loss


#file = open("./dataset/descr.json", "w")
#json.dump({
#    "0.jpg": [0.2, 0.14],
#    "1.jpg": [0.3, 0.78],
#    "2.jpg": [0.4, 0.2],
#    "3.jpg": [0.5, 0.5],
#    "4.jpg": [0.6, 0.7]
#    },file)
#file.close()

ITERS = 500

Extractor = Style_Extractor()
#Extractor.extractor.load("network_extractor.h5")

img = Images("./dataset/animehq/")
images,latents = img.getImages(Extractor.resolution,Extractor.resolution,0,5)
print(images.shape,latents.shape)

for i in range(ITERS):
    loss = Extractor.train_step(images,latents)
    print("Iteration: {} Loss: {}".format(i,loss))

Extractor.extractor.save("network_extractor.h5")


