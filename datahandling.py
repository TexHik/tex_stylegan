import cv2
import numpy as np
import os
import time
import json
from PIL import Image

class Images():
    def __init__(self,dir):
        self.dir = dir
        self.files = np.array([i for i in os.listdir(dir)], dtype='str')
        self.lenldr = len(self.files)
        if (os.path.exists(dir+"../latents.json")):
            with open(dir+"../latents.json") as file:
                self.latents = json.load(file)
        else:
            self.latents = None

        self.permutation = np.random.permutation(self.lenldr)
    def getImages(self, xsize, ysize,ptrs=0, ptre=0):

        assert ptre>ptrs
        assert ptre<=self.lenldr
        img = (np.array([cv2.resize(cv2.imread(self.dir+self.files[self.permutation[i+ptrs]]), (xsize,ysize),interpolation=cv2.INTER_LINEAR) for i in range(ptre-ptrs)],dtype='float32')-127.5)/127.5
        latents = None
        if self.latents!=None:
            latents = np.array([self.latents[self.files[self.permutation[i+ptrs]]] for i in range(ptre-ptrs)],dtype='float32').reshape(-1,1)
        return img.reshape(-1,xsize,ysize,3), latents
    def show(self,data):
        img = Image.fromarray(data, 'RGB')
        img.show()